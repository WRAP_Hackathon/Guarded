package com.example.root.wrapguarded.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;
import android.content.SharedPreferences;

import com.example.root.wrapguarded.GPS.TrackGPS;
import com.example.root.wrapguarded.R;
import com.example.root.wrapguarded.ResultActivity;
import com.example.root.wrapguarded.communication.RestPositionSender;
import com.example.root.wrapguarded.model.Position;
import com.example.root.wrapguarded.model.SimpleTask;
import com.example.root.wrapguarded.model.SimpleTaskComparable;
import com.example.root.wrapguarded.util.TaskManager;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by root on 09.06.17.
 */

public class NotificationReceiver {


    public static void parseNotifications(Context c) {
        List<SimpleTask> taskList;
        taskList = TaskManager.INSTANCE.getTasksForToday();
        Collections.sort(taskList, new SimpleTaskComparable());
        int minCheck, hourCheck;
        boolean checkTime = true;
        Calendar now = Calendar.getInstance();
        minCheck = now.get(Calendar.MINUTE);
        hourCheck = now.get(Calendar.HOUR_OF_DAY);
        Iterator<SimpleTask> it = taskList.iterator();
        while (it.hasNext()) {
            SimpleTask task = it.next();

            if (hourCheck > task.getHours()) {
                it.remove();
            } else if (hourCheck == task.getHours()) {
                if (minCheck > task.getMinute()) {
                    it.remove();
                } else {
                    break;
                }
            }
        }

        NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(c)
                .setSmallIcon(R.drawable.ic_face_black_24dp)
                .setContentTitle(taskList.get(0).getName())
                .setContentText(taskList.get(0).getName())
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setLights(Color.RED, 3000, 3000);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);

        Intent resultIntent = new Intent(c, ResultActivity.class);
        resultIntent.putExtra("itemid", taskList.get(0).getId());
        resultIntent.putExtra("item", taskList.get(0).getName());
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(c);
        stackBuilder.addParentStack(ResultActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
        if (taskList.get(0).getHours() == hourCheck && taskList.get(0).getMinute() <= minCheck + 1)
            mNotificationManager.notify(0, mBuilder.build());
        TrackGPS gps;
        double longitude = 0;
        double latitude = 0;
        gps = new TrackGPS(c);

        if (gps.canGetLocation()) {


            longitude = gps.getLongitude();
            latitude = gps.getLatitude();
        }

        //Toast.makeText(c, "Lat: " + latitude + " Lon: " + longitude, Toast.LENGTH_SHORT);

        Position myPos = new Position(0,new Date(),latitude, longitude);
        RestPositionSender.addPosition(myPos);
        Log.d("elo", "Lat: " + latitude + " Lon: " + longitude);
    }


}

//    @Override
//    public void onReceive(Context context, Intent intent) {
//        Toast.makeText(context, "Don't panik but your time is up!!!!.",
//                Toast.LENGTH_LONG).show();
//        // Vibrate the mobile phone
//        /*Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
//        vibrator.vibrate(2000);*/
//        Log.d("jolo", "hehe");
//        List<SimpleTask> TaskList = new ArrayList<>();
//        sharedPreferences = context.getSharedPreferences("WrapGuarded", Context.MODE_PRIVATE);
//
//        TaskList = TaskManager.INSTANCE.getTasksForToday();
//        Collections.sort(TaskList, new SimpleTaskComparable());
//        Calendar now = Calendar.getInstance();
//        Minute = now.get(Calendar.MINUTE);
//        Hours = now.get(Calendar.HOUR);
//        while(checkTime = true){
//            if(TaskList.get(0).getHours()<=Hours){
//                if(TaskList.get(0).getMinute()<Minute){
//                    TaskList.remove(0);
//                } else  checkTime = true;
//            }else checkTime = true;
//        }
//
//        final NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.ic_face_black_24dp)
//                .setContentTitle("Hello")
//                .setContentText("Click me bitch")
//                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
//                .setLights(Color.RED, 3000, 3000);
//        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        mBuilder.setSound(alarmSound);
//
//        Intent resultIntent = new Intent(context, ResultActivity.class);
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
//        stackBuilder.addParentStack(ResultActivity.class);
//        stackBuilder.addNextIntent(resultIntent);
//        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//        mBuilder.setContentIntent(resultPendingIntent);
//        final NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        if (sharedPreferences.getBoolean("RunNotify", true) == true) {
//            mNotificationManager.notify(0, mBuilder.build());
//        }
//    }
