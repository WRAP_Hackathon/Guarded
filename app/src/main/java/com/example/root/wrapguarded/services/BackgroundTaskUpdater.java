package com.example.root.wrapguarded.services;

import android.app.Service;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.example.root.wrapguarded.communication.RestMessageRetriever;
import com.example.root.wrapguarded.communication.RestScheduleRetriever;
import com.example.root.wrapguarded.model.Location;
import com.example.root.wrapguarded.model.Notification;
import com.example.root.wrapguarded.model.Schedule;
import com.example.root.wrapguarded.model.SimpleTask;
import com.example.root.wrapguarded.util.MessagesManager;
import com.example.root.wrapguarded.util.TaskManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by APS Drones on 09.06.2017.
 */

public class BackgroundTaskUpdater extends Service {

    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            try {
                TaskManager.INSTANCE.updateSchedule(getTodaysSchedule());
                NotificationReceiver.parseNotifications(getApplicationContext());
                MessagesManager.INSTANCE.setMessages(getNotifications());

            } catch (Exception e) {
//                Toast.makeText(BackgroundTaskUpdater.this, "Error parsing tasks: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
            timerHandler.postDelayed(this, 5000);
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        timerHandler.postDelayed(timerRunnable, 0);
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Schedule getTodaysSchedule() throws JSONException, ParseException {
        JSONObject scheduleObj = RestScheduleRetriever.getSchedule().getJSONObject("returnable");
        JSONArray tasks = scheduleObj.getJSONArray("tasks");

        Schedule schedule = new Schedule();
        schedule.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(scheduleObj.getString("date")));

        for (int i = 0; i < tasks.length(); i++) {
            JSONObject singleTask = tasks.getJSONObject(i);
            SimpleTask singleTaskInstance = new SimpleTask();
            singleTaskInstance.setName(singleTask.getString("name"));
            singleTaskInstance.setSchedule(schedule);
            singleTaskInstance.setId(singleTask.getInt("task_id"));
            singleTaskInstance
                    .setTime(new SimpleDateFormat("HH:mm").parse(singleTask.getString("time")));
            if (singleTask.has("location")) {
                JSONObject locationObj = singleTask.getJSONObject("location");

                double lat = locationObj.getDouble("latitude");
                double lon = locationObj.getDouble("longitude");

                Location location = new Location(lat, lon);
                singleTaskInstance.setLocation(location);
            }

            schedule.getTasks().add(singleTaskInstance);
        }

        return schedule;
    }
    private List<Notification> getNotifications() throws JSONException, ParseException {
        JSONArray positions = RestMessageRetriever.getMessages().getJSONArray("instance");

        List<Notification> notifications = new LinkedList<>();

        for (int i = 0; i < positions.length(); i++) {
            JSONObject singleTask = positions.getJSONObject(i);
            Notification singleTaskInstance = new Notification();
            singleTaskInstance.setMessage(singleTask.getString("msg"));
            singleTaskInstance.setSeen(singleTask.getBoolean("seen"));
            singleTaskInstance.setToGuardian(singleTask.getBoolean("toGuardian"));
            singleTaskInstance.setTime(new SimpleDateFormat("yyyy/MM/dd HH:mm").parse(singleTask.getString("date")));

            notifications.add(singleTaskInstance);
        }

        return notifications;
    }

}
