package com.example.root.wrapguarded.util;

import com.example.root.wrapguarded.model.Notification;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 10.06.17.
 */

public enum MessagesManager {
    INSTANCE;
    List<Notification> messages = new ArrayList<>();

    public List<Notification> getMessages(){
        return messages;
    }

    public void setMessages(List<Notification> nsg){
        this.messages = nsg;
    }

}
