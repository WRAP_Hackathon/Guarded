package com.example.root.wrapguarded.util;

import com.example.root.wrapguarded.model.Schedule;
import com.example.root.wrapguarded.model.SimpleTask;

import java.util.List;

/**
 * Created by APS Drones on 09.06.2017.
 */

public enum TaskManager {
    INSTANCE;

    private Schedule todaysSchedule;

    public List<SimpleTask> getTasksForToday() {
        return todaysSchedule.getTasks();
    }

    public void updateSchedule(Schedule schedule) {
        todaysSchedule = schedule;
    }
}
