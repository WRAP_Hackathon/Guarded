package com.example.root.wrapguarded.services;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;

/**
 * Created by APS Drones on 09.06.2017.
 */

public class RestUpdater extends JobService{

    private ScheduleUpdateTask updateTask = new ScheduleUpdateTask();

    @Override
    public boolean onStartJob(JobParameters params) {
        updateTask.execute(params);

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        boolean shouldReschedule = updateTask.stopJob(params);

        return shouldReschedule;
    }

    private class ScheduleUpdateTask extends AsyncTask<JobParameters, Void, JobParameters[]> {
        @Override
        protected JobParameters[] doInBackground(JobParameters... params) {

            return params;
        }

        @Override
        protected void onPostExecute(JobParameters[] result) {
            for (JobParameters params : result) {
                if (!hasJobBeenStopped(params)) {
                    jobFinished(params, false);
                }
            }
        }

        private boolean hasJobBeenStopped(JobParameters params) {
            return true;
        }

        public boolean stopJob(JobParameters params) {
            return true;
        }
    }
}
