package com.example.root.wrapguarded.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by APS Drones on 09.06.2017.
 */

public class SimpleTaskComparable implements Comparator<SimpleTask> {
    @Override
    public int compare(SimpleTask t1, SimpleTask t2) {
        if (t1.getHours() == t2.getHours()) { // jeśli godziny równe to porównujemy minuty
            if (t1.getMinute() > t2.getMinute()) {    // 1 jeśli t1 większy
                return 1;
            } else if (t1.getMinute() < t2.getMinute()) {  // -1 jesli t2 wiekszy
                return -1;
            } else {
                return 0; // 0 jeśli równe
            }
        } else if (t1.getHours() > t2.getHours()) { // 1 jeśli t1 wiekszy
            return 1;
        } else if (t1.getHours() < t2.getHours()) {
            return -1;
        }
        return 0;
    }

    // użycie
//    Collections.sort(list, new SimpleTaskComparable());
}
