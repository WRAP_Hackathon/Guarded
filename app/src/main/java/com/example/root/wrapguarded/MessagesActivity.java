package com.example.root.wrapguarded;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.root.wrapguarded.model.Notification;
import com.example.root.wrapguarded.util.MessagesManager;
import com.example.root.wrapguarded.util.TaskManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessagesActivity extends AppCompatActivity {

    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            try {
                adapter.clear();
                List<Notification> notifications  = MessagesManager.INSTANCE.getMessages();
                for(Notification n : notifications){
                    String time = new SimpleDateFormat("HH:mm").format(n.getTime());
                    if(n.isToGuardian())
                        adapter.add(time + " ->"+n.getMessage());
                    else
                        adapter.add(time+" "+n.getMessage());

                }

            } catch (Exception e) {
                Toast.makeText(MessagesActivity.this, "Messages coulnd't have been added.", Toast.LENGTH_SHORT).show();
            }
            timerHandler.postDelayed(this, 3000);
        }
    };
    @BindView(R.id.list)
            protected ListView listView;
    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        ButterKnife.bind(this);

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                new ArrayList<String>());
        listView.setAdapter(adapter);

        timerHandler.postDelayed(timerRunnable, 0);
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            timerHandler.removeCallbacks(timerRunnable);
        }catch (Exception e){

        }
    }
}
