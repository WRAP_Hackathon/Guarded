package com.example.root.wrapguarded;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.root.wrapguarded.model.SimpleTask;
import com.example.root.wrapguarded.util.TaskManager;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskViewActivity extends AppCompatActivity {

    @BindView(R.id.textName)
    protected TextView name;

    @BindView(R.id.textWhen)
    protected TextView when;

    @BindView(R.id.textTimeLeft)
    protected TextView timeLeft;

    private long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_view);
        ButterKnife.bind(this);

        List<SimpleTask> list = TaskManager.INSTANCE.getTasksForToday();
        if (getIntent().hasExtra("taskId")) {
            int id = getIntent().getIntExtra("taskId", -1);

            if (id == -1) {
                Toast.makeText(this, "Couldn't find task.", Toast.LENGTH_SHORT).show();
                finish();
            }

            for (SimpleTask task : list) {
                if (task.getId() == id) {
                    name.setText("Nazwa: " + task.getName());

                    Date dNow = new Date();
                    Date dThen = new Date();
                    dThen.setHours(task.getHours());
                    dThen.setMinutes(task.getMinute());

                    long diffMinutes = getDateDiff(dNow, dThen, TimeUnit.MINUTES);
                    long diffHours = getDateDiff(dNow, dThen, TimeUnit.HOURS);

                    String min = String.valueOf(diffMinutes);
                    if (min.length() == 1) {
                        min = "0" + min;
                    }

                    String hr = String.valueOf(diffHours);
                    if (hr.length() == 1) {
                        hr = "0" + hr;
                    }

                    timeLeft.setText("Zostało " + hr + " h i " + min + " min");
                    when.setText("Planowany czas: " + task.getHours() + ":" + task.getMinute());
                    break;
                }
            }
        }
    }
}
