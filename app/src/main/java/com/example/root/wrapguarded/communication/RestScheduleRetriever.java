package com.example.root.wrapguarded.communication;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

/**
 * Created by APS Drones on 09.06.2017.
 */

public class RestScheduleRetriever extends RestClientBase {
    private String date;
//    private String token;

    private RestScheduleRetriever(Date date) {
        super("POST", "http://172.16.102.35:8080/WrapGuardianServer/getScheduleForToday");
        this.date = new SimpleDateFormat("yyyy/MM/dd").format(date);
//        this.token = SessionManager.getInstance().getToken();
    }

    private RestScheduleRetriever() {
        this(new Date());
    }

    protected void prepare() {
        JSONObject obj = new JSONObject();

        try {
            obj.put("date", String.valueOf(date));
//            obj.put("token", token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setData(obj);
    }

    public static JSONObject getSchedule(Date date) {
        JSONObject response = null;
        RestScheduleRetriever task = null;
        try {
            task = new RestScheduleRetriever();
            task.prepare();
            task.execute();
            response = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return response;
    }

    public static JSONObject getSchedule() {
        return getSchedule(new Date());
    }
}