package com.example.root.wrapguarded.communication;

import com.example.root.wrapguarded.model.Position;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

/**
 * Created by APS Drones on 09.06.2017.
 */

public class RestPositionSender extends RestClientBase {

    private Position position;

    private RestPositionSender(Position position) {
        super("POST", "http://172.16.102.35:8080/WrapGuardianServer/message/addPosition");
        this.position = position;
    }

    protected void prepare() {
        try {
            JSONObject obj = position.toJSON();
            setData(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JSONObject addPosition(Position msg) {
        JSONObject response = null;
        RestPositionSender task = null;
        try {
            task = new RestPositionSender(msg);
            task.prepare();
            task.execute();
            response = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return response;
    }
}