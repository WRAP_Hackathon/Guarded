package com.example.root.wrapguarded.model;

import org.json.JSONException;
import org.json.JSONObject;

public interface  IJSONParsable {
	public JSONObject toJSON() throws JSONException;

}
