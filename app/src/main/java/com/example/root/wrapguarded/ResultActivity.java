package com.example.root.wrapguarded;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.root.wrapguarded.model.SimpleTask;
import com.example.root.wrapguarded.util.TaskManager;
import com.google.android.gms.location.LocationListener;

import java.util.List;

public class ResultActivity extends AppCompatActivity {


//    SharedPreferences sharedPreferences;
//    SharedPreferences.Editor editor;
    TextView taskName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        taskName = (TextView)findViewById(R.id.taskName);
//        sharedPreferences = getSharedPreferences("WrapGuarded", MODE_PRIVATE);
//        editor = sharedPreferences.edit();
//        editor.putBoolean("RunNotify", false);
//        editor.commit();

        List<SimpleTask> list = TaskManager.INSTANCE.getTasksForToday();
        if (getIntent().hasExtra("itemid")) {
            int id = getIntent().getIntExtra("itemid", -1);

            if (id == -1) {
                Toast.makeText(this, "Couldn't find task.", Toast.LENGTH_SHORT).show();
                finish();
            }

            for (SimpleTask task : list) {
                if (task.getId() == id) {
                    taskName.setText(task.getName());
                }
            }
        }
    }

    public void No(View view) {
        finish();
    }

    public void Yes(View view) {
        finish();
    }

    public void Dontknow(View view) {
        finish();
    }
}
