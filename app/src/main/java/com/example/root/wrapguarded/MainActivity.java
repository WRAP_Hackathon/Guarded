package com.example.root.wrapguarded;


import android.Manifest;
import android.app.AlarmManager;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.root.wrapguarded.communication.RestMessageSender;
import com.example.root.wrapguarded.model.Notification;
import com.example.root.wrapguarded.model.SimpleTask;
import com.example.root.wrapguarded.model.SimpleTaskComparable;
import com.example.root.wrapguarded.services.NotificationReceiver;
import com.example.root.wrapguarded.services.BackgroundTaskUpdater;
import com.example.root.wrapguarded.services.RestUpdater;
import com.google.android.gms.location.LocationListener;
import com.example.root.wrapguarded.util.TaskManager;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class MainActivity extends AppCompatActivity {
    int i =0;
    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            try {
                tasksAdapter.clear();
                tasksAdapter.addAll(TaskManager.INSTANCE.getTasksForToday());
            } catch (Exception e) {
//                Toast.makeText(MainActivity.this, "Tasks coulnd't have been added.", Toast.LENGTH_SHORT).show();
            }
            timerHandler.postDelayed(this, 3000);
        }
    };

    @BindView(R.id.listView)
    protected ListView listView;

    private ArrayAdapter<SimpleTask> tasksAdapter;

    @OnItemClick(R.id.listView)
    protected void itemClick(int position) {
        SimpleTask task = tasksAdapter.getItem(position);

        Intent intent = new Intent(this, TaskViewActivity.class);
        intent.putExtra("taskId", task.getId());

        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        tasksAdapter = new ArrayAdapter<SimpleTask>(this,
                android.R.layout.simple_list_item_1, new ArrayList<SimpleTask>());

        listView.setAdapter(tasksAdapter);

        if (!isMyServiceRunning(BackgroundTaskUpdater.class)) {
            getApplicationContext().startService(new Intent(getApplicationContext(), BackgroundTaskUpdater.class));
        }

        timerHandler.postDelayed(timerRunnable, 2000);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void Help(View view) {
            // TODO Auto-generated method stub
            i++;
            Handler handler = new Handler();
            Runnable r = new Runnable() {

                @Override
                public void run() {
                    i = 0;
                }
            };

            if (i == 1) {
                //Single click
                handler.postDelayed(r, 250);
            } else if (i == 2) {
                //Double click
                Log.d("hello", "double click");
                Notification m = new Notification();
                m.setTime(new Date());
                m.setMessage("Potrzebuje pomocy");
                m.setSeen(false);
                m.setToGuardian(true);
                RestMessageSender.sendMessage(m);
                Toast.makeText(this, "Wiadomość została wysłana", Toast.LENGTH_SHORT).show();
                i = 0;



        }
    }

    public void wiadomosci(View view) {
        Intent intent = new Intent(this, MessagesActivity.class);
        startActivity(intent);
    }
}
